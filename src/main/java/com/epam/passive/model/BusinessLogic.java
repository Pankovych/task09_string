package com.epam.passive.model;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.*;

public class BusinessLogic implements Model {

    private static Logger logger1 = LogManager.getLogger(BusinessLogic.class);

    public BusinessLogic() {
    }

    public void maxWithLamda(int fa, int fb, int fc) {
        IFunctional lamda = (a, b, c) -> a > b ? Math.max(a, c) : Math.max(b, c);
        logger1.info("Max is " + lamda.fun(fa, fb, fc));
    }

    public void averageWithLamda(int fa, int fb, int fc) {
        IFunctional lamda = (a, b, c) -> (a + b + c) / 3;
        logger1.info("Average is " + lamda.fun(fa, fb, fc));
    }

    public void checkCapitalLetters(String message) {

    }

    public void replaceVowels(String message) {
        String pattern="[auoieqy]";
        logger1.info(message.replaceAll(pattern,"_"));
    }

    public void forthTask(String message){
        List<String> splitString = Arrays.asList(message.split(" "));

        for(String n:splitString){
            System.out.println(" "+n);
        }
    }


}
