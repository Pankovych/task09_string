package com.epam.passive.model;

@FunctionalInterface
public interface IFunctional {
    int fun(int a,int b,int c);
}
