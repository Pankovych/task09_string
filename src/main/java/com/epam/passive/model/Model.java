package com.epam.passive.model;

import java.util.List;

public interface Model {

    void maxWithLamda(int fa, int fb, int fc);
    void averageWithLamda(int fa, int fb, int fc);
    void checkCapitalLetters(String message);
    void replaceVowels(String Message);
    void forthTask(String message);

}
