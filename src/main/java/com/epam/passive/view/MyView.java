package com.epam.passive.view;

import com.epam.passive.controller.*;
import org.apache.logging.log4j.*;

import java.util.*;

public class MyView {
    private static Logger logger1 = LogManager.getLogger(MyView.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private Locale locale;
    private ResourceBundle bundle;
    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("Q", bundle.getString("Q"));
    }
    public MyView() {
        controller = new ControllerImpl();
        locale = new Locale("uk");
        bundle=ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::internationalizeMenuUkrainian);
        methodsMenu.put("6", this::internationalizeMenuEnglish);
    }

    private  void internationalizeMenuUkrainian(){
        locale = new Locale("uk");
        bundle=ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        show();
    }

    private  void internationalizeMenuEnglish(){
        locale = new Locale("en");
        bundle=ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        show();
    }

    private void pressButton1() {
        logger1.info("Input message");
        String message = input.nextLine();
        controller.firstTask(message);
    }

    private void pressButton2() {
        logger1.info("Input message");
        String message = input.nextLine();
        controller.secondTask(message);
    }

    private void pressButton3() {
        controller.thirdTask();
    }
    private void pressButton4()
    {
        logger1.info("Input message");
        String text = input.nextLine();
        controller.fourthTask(text);
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
