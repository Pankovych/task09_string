package com.epam.passive.controller;

import com.epam.passive.model.*;
import org.apache.logging.log4j.*;

import java.util.List;
import java.util.OptionalDouble;

public class ControllerImpl implements Controller {
    private Model model;
    private static Logger logger1 = LogManager.getLogger(BusinessLogic.class);

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public final void firstTask(String message) {
        model.checkCapitalLetters(message);
    }

    @Override
    public final void secondTask(String message) {
        model.replaceVowels(message);
    }

    @Override
    public final void thirdTask() {

    }

    @Override
    public void fourthTask(String message){
        model.forthTask(message);
    }
}
